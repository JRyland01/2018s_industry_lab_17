package ictgradschool.industry.testingandrefactoring.ex04;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    private static final Random r = new Random();
    private Canvas c = new Canvas();
    private int foodX = -1, foodY = -1;
    private ArrayList<Integer> redBlockX = new ArrayList<>();
    private ArrayList<Integer> snakeXDirection = new ArrayList<>();
    private ArrayList<Integer> redBlockY = new ArrayList<>();
    private ArrayList<Integer> snakeYDirection = new ArrayList<>();
    private int d; //arrow key press
    private boolean done = false;

    public static void main(String[] args) {
        new Program().go();
    }

    public Program() {
        for (int i = 0; i < 6; i++) {
            snakeXDirection.add(10 - i);
            snakeYDirection.add(10);
        }
        this.d = 39;
        setTitle("Program" + " : " + 6);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        c.setBackground(Color.white);
        add(BorderLayout.CENTER, c);
        //add key listener, for if key pressed is not a arrow key
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int d1 = e.getKeyCode();
                if ((d1 >= 37) && (d1 <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.d - d1) != 2) {// block moving back
                        Program.this.d = d1;
                    }
                }
            }
        });
        setVisible(true);
    }

    void go() { // main loop
        while (!done) {
            int x2 = snakeXDirection.get(0);
            int y2 = snakeYDirection.get(0);
            if (d == 37) {
                x2--;
            }
            if (d == 39) {
                x2++;
            }
            if (d == 38) {
                y2--;
            }
            if (d == 40) {
                y2++;
            }
            if (x2 > 30 - 1) {
                x2 = 0;
            }
            if (x2 < 0) {
                x2 = 30 - 1;
            }
            if (y2 > 20 - 1) {
                y2 = 0;
            }
            if (y2 < 0) {
                y2 = 20 - 1;
            }
            //possible break
            boolean redBlockHit = false;
            boolean snakeHeadHit = false;
            //hit redblock and stop
            for (int i1 = 0; i1 < redBlockX.size(); i1++) {
                if (redBlockX.get(i1) == x2 && redBlockY.get(i1) == y2) {
                    redBlockHit = true;
                }
            }
            for (int i1 = 0; i1 < snakeXDirection.size(); i1++) {
                if ((snakeXDirection.get(i1) == x2) && (snakeYDirection.get(i1) == y2)) {
                    if (!((snakeXDirection.get(snakeXDirection.size() - 1) == x2) && (snakeYDirection.get(snakeYDirection.size() - 1) == y2))) {
                        snakeHeadHit = true;
                    }
                }
            }
            //possible break
            done = redBlockHit || snakeHeadHit;
            snakeXDirection.add(0, x2);
            snakeYDirection.add(0, y2);
            //eating food
            if (((snakeXDirection.get(0) == foodX) && (snakeYDirection.get(0) == foodY))) {
                foodX = -1;
                foodY = -1;
                setTitle("Program" + " : " + snakeXDirection.size());
            } else {
                snakeXDirection.remove(snakeXDirection.size() - 1);
                snakeYDirection.remove(snakeYDirection.size() - 1);
            }
            if (foodX == -1) {
                int x, y;
                boolean check1 = false;
                boolean check2 = false;
                do {
                    x = r.nextInt(30);
                    y = r.nextInt(20);
                    for (int i = 0; i < redBlockX.size(); i++) {
                        if (redBlockX.get(i) == x && redBlockY.get(i) == y) {
                            check1 = true;
                        }
                    }
                    for (int i = 0; i < snakeXDirection.size(); i++) {
                        if ((snakeXDirection.get(i) == x) && (snakeYDirection.get(i) == y)) {
                            if (!((snakeXDirection.get(snakeXDirection.size() - 1) == x) && (snakeYDirection.get(snakeYDirection.size() - 1) == y))) {
                                check2 = true;
                            }
                        }
                    }
                }
                //possible break
                while (check2 || check1);
                foodX = x;
                foodY = y;
                int x1, y1;
                boolean check3 = false;
                boolean check4 = false;
                do {
                    x1 = r.nextInt(30);
                    y1 = r.nextInt(20);
                    for (int i = 0; i < redBlockX.size(); i++) {
                        if (redBlockX.get(i) == x1 && redBlockY.get(i) == y1) {
                            check3 = true;
                        }
                    }
                    for (int i = 0; i < snakeXDirection.size(); i++) {
                        if ((snakeXDirection.get(i) == x1) && (snakeYDirection.get(i) == y1)) {
                            if (!((snakeXDirection.get(snakeXDirection.size() - 1) == x1) && (snakeYDirection.get(snakeYDirection.size() - 1) == y1))) {
                                check4 = true;
                            }
                        }
                    }
                } while (check3 || check4 || foodX == x1 && foodY == y1);

                redBlockX.add(x1);
                redBlockY.add(y1);
            }
            c.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            //drawing snake
            for (int i = 0; i < snakeXDirection.size(); i++) {
                g.setColor(Color.gray);
                g.fill3DRect(snakeXDirection.get(i) * 25 + 1, snakeYDirection.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            //drawing food
            g.setColor(Color.green);
            g.fill3DRect(foodX * 25 + 1, foodY * 25 + 1, 25 - 2, 25 - 2, true);
            //draw block
            for (int i = 0; i < redBlockX.size(); i++) {
                g.setColor(Color.red);
                g.fill3DRect(redBlockX.get(i) * 25 + 1, redBlockY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            //game over
            if (done) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }
    }
}