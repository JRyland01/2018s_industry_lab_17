package ictgradschool.industry.testingandrefactoring.ex03;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {
    private String line = "";
    private String output = "";
    private int classSize = 550;
    private final int numLabs = 3;
    private ArrayList<String> firstNameList = new ArrayList<String>();
    private ArrayList<String> surnameList = new ArrayList<String>();
    private String student = "";

    public void ExcelNew(){
        readTheFile();
        output = theWorkHorse();
        writeTheMagic(output);
    }
    public void readTheFile() {
        try {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader("FirstNames.txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            while ((line = br.readLine()) != null) {
                firstNameList.add(line);
            }
            br.close();
            br = new BufferedReader(new FileReader("Surnames.txt"));
            while ((line = br.readLine()) != null) {
                surnameList.add(line);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void writeTheMagic(String output){
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
            bw.write(output);
            bw.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        public int randomStudentSkill(){
        return (int) (Math.random() * 101);
        }
        public String theWorkHorse() {
            for (int i = 1; i <= classSize; i++) {
                if (i / 10 < 1) {
                    student += "000" + i;
                } else if (i / 100 < 1) {
                    student += "00" + i;
                } else if (i / 1000 < 1) {
                    student += "0" + i;
                } else {
                    student += i;
                }
                int randFNIndex = (int) (Math.random() * firstNameList.size());
                int randSNIndex = (int) (Math.random() * surnameList.size());
                student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
                //Student Skill
                int randStudentSkill = randomStudentSkill();
                //Labs//////////////////////////
                lab(randStudentSkill);
                //Test/////////////////////////
                test(randStudentSkill);
                ///////////////Exam////////////
                exam(randStudentSkill);
                //////////////////////////////////
                student += "\n";
                output += student;
            }
            return output;
        }
    public void lab(int r){
        for (int j = 0; j < numLabs; j++) {
            if (r <= 5) {
                student += (int) (Math.random() * 40); //[0,39]
            } else if ((r > 5) && (r <= 15)) {
                student += ((int) (Math.random() * 10) + 40); // [40,49]
            } else if ((r > 15) && (r <= 25)) {
                student += ((int) (Math.random() * 20) + 50); // [50,69]
            } else if ((r > 25) && (r<= 65)) {
                student += ((int) (Math.random() * 20) + 70); // [70,89]
            } else {
                student += ((int) (Math.random() * 11) + 90); //[90,100]
            }
            student += "\t";
        }
    }
    public void test(int r) {
        if (r <= 5) {
            student += (int) (Math.random() * 40); //[0,39]
        } else if ((r > 5) && (r <= 20)) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((r > 20) && (r <= 65)) {
            student += ((int) (Math.random() * 20) + 50); //[50,69]
        } else if ((r > 65) && (r <= 90)) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        student += "\t";
    }
    public void exam(int r){
        if (r <= 7) {
            int randDNSProb = (int) (Math.random() * 101);
            if (randDNSProb <= 5) {
                student += ""; //DNS
            } else {
                student += (int) (Math.random() * 40); //[0,39]
            }
        } else if ((r > 7) && (r <= 20)) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((r > 20) && (r <= 60)) {
            student += ((int) (Math.random() * 20) + 50);//[50,69]
        } else if ((r > 60) && (r <= 90)) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
    }
    public static void main(String[] args) {
        ExcelNew e = new ExcelNew();
        e.ExcelNew();
    }

}



