package ictgradschool.industry.testingandrefactoring.ex02;

import java.util.Scanner;

public class ShapeAreaCalculater {
    private double width;
    private double length;
    private double radiusC;
    private int areaOfCirle;
    private int areaOfRectangle;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getRadius() {
        return radiusC;
    }

    public void setRadius(double radiusC) {
        this.radiusC = radiusC;
    }

    public void shapeAreaCalculater() {
        System.out.println("Welcome to the Shape Area Calculator!\n");
        System.out.println("Enter the width of rectangle: ");
        width = keyboard();
        System.out.println("Enter the length of the rectangle: ");
        length = keyboard();
        System.out.println();
        System.out.println("The radius of the circle: ");
        radiusC = keyboard();
        System.out.println();
        System.out.println("The smaller area is : ");
        areaOfCirle = getCircleArea(radiusC);
        areaOfRectangle = getArea(width,length);
        int smallerArea = theSmallest();
        System.out.println(smallerArea);
    }
        public int getCircleArea(double radiusC){
            return (int)(Math.round(Math.PI *(radiusC*radiusC)));
        }
        public String convertToString ( double d){
            return Double.toString(d);
        }
        public int getArea ( double width, double length){

            return (int)(width*length);
        }
        public double keyboard(){
            Scanner keyboard = new Scanner(System.in);
            double d = keyboard.nextDouble();
            return d;
        }
        public int theSmallest(){
        return Math.min(areaOfCirle,areaOfRectangle);
        }
        public static void main (String[]args){
            ShapeAreaCalculater go = new ShapeAreaCalculater();
            go.shapeAreaCalculater();
        }

    public int getAreaOfRectangle() {
        return areaOfRectangle;
    }

    public void setAreaOfRectangle(int areaOfRectangle) {
        this.areaOfRectangle = areaOfRectangle;
    }

    public int getAreaOfCirle() {
        return areaOfCirle;
    }

    public void setAreaOfCirle(int areaOfCirle) {
        this.areaOfCirle = areaOfCirle;
    }
}



