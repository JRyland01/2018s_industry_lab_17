package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ShapeTest {
    private double areaOfRect = 100;
    private int areaOfCircle = 314;
    private  double length = 10.00;
    private double radius = 10.00;
    private double width =  10.5;
    private ShapeAreaCalculater shapeAreaCalculater;
    @Before
    public void myShape(){
        shapeAreaCalculater = new ShapeAreaCalculater();
    }

    @Test
    public void testConversionToString(){
        assertEquals("10.5",shapeAreaCalculater.convertToString(width));
    }
    @Test
    public void testAreaOfRectangle(){
        assertTrue(width*length - shapeAreaCalculater.getArea(width,length) < 1e-15);
    }
    @Test
    public void testAreaOfCircle(){
        assertEquals(areaOfCircle,shapeAreaCalculater.getCircleArea(radius));
    }
}
