package ictgradschool.industry.testingandrefactoring.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }
    @Test
    public void turnEast(){
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
    }
    @Test
    public void turnSouth(){
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
    }
    @Test
    public void turnWest(){
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
    }
    @Test
    public void turnThreeSixty(){
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
    }
    @Test
    public void moveNorth(){
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        assertEquals(9,myRobot.row());
        assertEquals(1,myRobot.column());
    }
    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }
    @Test
    public void moveEast(){
        myRobot.turn();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        assertEquals(2,myRobot.column());
        assertEquals(10,myRobot.row());
    }
    @Test
    public void testIllegalMoveEast() {
        boolean atEasternEdge = false;
        myRobot.turn();
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atEasternEdge = myRobot.currentState().column == 10;
            assertTrue(atEasternEdge);
        } catch (IllegalMoveException e) {
            fail();
        }
        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().column);
        }
    }
    @Test
    public void moveWest(){
        myRobot.turn();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        myRobot.turn();
        myRobot.turn();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        assertEquals(1,myRobot.column());
        assertEquals(10,myRobot.row());
    }
    @Test
    public void testIllegalMoveWest() {
        boolean atWesternEdge = false;
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            assertEquals(1, myRobot.currentState().column);
        }
    }
    @Test
    public void moveSouth(){
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        myRobot.turn();
        myRobot.turn();
        try {
            myRobot.move();
        } catch (IllegalMoveException e) {
            fail();
        }
        assertEquals(1,myRobot.column());
        assertEquals(10,myRobot.row());
    }
    @Test
    public void testIllegalMoveSouth() {
        myRobot.turn();
        myRobot.turn();
        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().row);
        }
    }
}
